;;; -*- lexical-binding: t; -*-

;; GARBAGE COLLECTION

(use-package gcmh
  :config
  (gcmh-mode 1)
  (setq gcmh-cons-threshold 100000000))

;; NO-LITTERING

(use-package no-littering
  :straight t
  :config
  (require 'no-littering))

(defvar autosave-dir (concat "~/.config/emacs/auto-save" "/"))
(make-directory autosave-dir t)
(setq auto-save-file-name-transforms
      `(("\\(?:[^/]*/\\)*\\(.*\\)" ,(concat autosave-dir "\\1") t)))

;; BACKUPS

(defun twig/backup-file-name (fpath)
  "Return a new file path of a given file path.
If the new path's directories does not exist, create them."
  (let* ((backupRootDir "~/.config/emacs/emacs-backup/")
         (backupFilePath (replace-regexp-in-string "//" "/" (concat backupRootDir filePath "~") )))
    (make-directory (file-name-directory backupFilePath) (file-name-directory backupFilePath))
    backupFilePath))
(setq make-backup-file-name-function 'twig/backup-file-name)

;; DRAG-STUFF

(use-package drag-stuff
  :defer t
  :config
  (drag-stuff-global-mode 1))

;; SCROLLING

(pixel-scroll-precision-mode)
(setq pixel-scroll-precision-large-scroll-height 5.0)
;; (setq pixel-scroll-precision-use-momentum t)
;; (setq fast-but-imprecise-scrolling t)
;; (setq mouse-wheel-progressive-speed t)

;; GTK

(menu-bar-mode -1)
(tool-bar-mode -1)
(blink-cursor-mode -1)

;; LINE NUMBERS

(setq display-line-numbers-type 'relative)
(add-hook 'prog-mode-hook 'display-line-numbers-mode)
(add-hook 'text-mode-hook 'visual-line-mode)

;; EXTERNAL CHANGES

(global-auto-revert-mode t)

;; SELECTION DELETE

(delete-selection-mode 1)
