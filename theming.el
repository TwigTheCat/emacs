;;; -*- lexical-binding: t; -*-

;; THEME

(add-to-list 'custom-theme-load-path "~/.config/emacs/themes/")
(use-package doom-themes
  :config
  ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t) ; if nil, italics is universally disabled
  (load-theme 'fairy-forest t)

  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config))

;; FONT

(add-to-list 'default-frame-alist '(font . "CaskaydiaCove Nerd Font-12:semibold:antialias=true:hinting=true"))

;; CUSTOM

(custom-set-faces
 '(org-level-1 ((t (:inherit outline-1 :height 1.1))))
 '(org-level-2 ((t (:inherit outline-2 :height 1.1))))
 '(org-level-3 ((t (:inherit outline-3 :height 1.1))))
 '(org-level-4 ((t (:inherit outline-4 :height 1.1))))
 '(org-level-5 ((t (:inherit outline-5 :height 1.1))))
 '(org-level-6 ((t (:inherit outline-5 :height 1.1))))
 '(org-level-7 ((t (:inherit outline-5 :height 1.1)))))

(custom-set-variables
 '(custom-safe-themes
   '("4dd267a7349de7b377c73a8fc7ea99a8e90ddecbc88bd8a731331f5dd44ae596"
     default))
 '(eglot-ignored-server-capabilities '(:codeLensProvider :inlayHintProvider))
 '(package-selected-packages nil))
