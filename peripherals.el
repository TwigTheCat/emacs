;;; -*- lexical-binding: t; -*-

;; SEARCHING

(use-package consult
  :config
  (global-set-key (kbd "C-s") 'consult-isearch-history))

;; COMPLETION AT POINT

(use-package cape
  ;; Press C-c p ? to for help.
  :bind ("C-c p" . cape-prefix-map) ;; Alternative keys: M-p, M-+, ...
  :init
  ;; Add to the global default value of `completion-at-point-functions' which is
  ;; used by `completion-at-point'.  The order of the functions matters, the
  ;; first function returning a result wins.  Note that the list of buffer-local
  ;; completion functions takes precedence over the global list.
  ;; (add-hook 'completion-at-point-functions #'cape-dabbrev)
  (add-hook 'completion-at-point-functions #'cape-file)
  (add-hook 'completion-at-point-functions #'cape-elisp-block))

;; CORFU

(use-package corfu
  :config
  (setq corfu-auto t)
  (setq corfu-on-exact-match 'quit)
  (setq-default corfu-auto-delay 0.1)
  (setq corfu-preview-current 1)
  (global-corfu-mode)
  (corfu-popupinfo-mode)
  (setq corfu-popupinfo-delay '(0.5 . 0.5)))

;; VERTICO

(use-package vertico
  :custom
  (vertico-cycle t)
  (completion-styles '(substring orderless basic))
  :init
  (vertico-mode)
  :config
  (setq completion-category-defaults nil)
  (setq completion-category-overrides nil)
  (vertico-multiform-mode)
  (defvar +vertico-transform-functions nil)

  (cl-defmethod vertico--format-candidate :around
    (cand prefix suffix index start &context ((not +vertico-transform-functions) null))
    (dolist (fun (ensure-list +vertico-transform-functions))
      (setq cand (funcall fun cand)))
    (cl-call-next-method cand prefix suffix index start))
  
  (defun +vertico-highlight-directory (file)
    "If FILE ends with a slash, highlight it as a directory."
    (if (string-suffix-p "/" file)
        (propertize file 'face 'marginalia-file-priv-dir) ; or face 'dired-directory
      file))

  (defun sort-directories-first (files)
    ;; Still sort by history position, length and alphabetically
    (setq files (vertico-sort-history-length-alpha files))
    ;; But then move directories first
    (nconc (seq-filter (lambda (x) (string-suffix-p "/" x)) files)
           (seq-remove (lambda (x) (string-suffix-p "/" x)) files)))
  
  ;; function to highlight enabled modes similar to counsel-M-x
  (defun +vertico-highlight-enabled-mode (cmd)
    "If MODE is enabled, highlight it as font-lock-constant-face."
    (let ((sym (intern cmd)))
      (if (or (eq sym major-mode)
              (and
               (memq sym minor-mode-list)
               (boundp sym)))
          (propertize cmd 'face 'font-lock-constant-face)
        cmd)))

  ;; add-to-list works if 'file isn't already in the alist
  ;; setq can be used but will overwrite all existing values
  (add-to-list 'vertico-multiform-categories
               '(file
                 ;; this is also defined in the wiki, uncomment if used
                 (vertico-sort-function . sort-directories-first)
                 (+vertico-transform-functions . +vertico-highlight-directory)))
  (add-to-list 'vertico-multiform-commands
               '(execute-extended-command 
                 reverse
                 (+vertico-transform-functions . +vertico-highlight-enabled-mode))))

;; ORDERLESS

(use-package orderless
  :custom
  (completion-styles '(orderless basic))
  (completion-category-overrides '((file (styles basic partial-completion)))))

;; MODELINE

(use-package nano-modeline
  :init
  (setq-default mode-line-format nil)
  (setq nano-modeline-position 'nano-modeline-footer)
  :config
  (nano-modeline-prog-mode t)
  (add-hook 'text-mode-hook #'nano-modeline-text-mode)
  (add-hook 'prog-mode-hook #'nano-modeline-prog-mode)
  (add-hook 'org-mode-hook #'nano-modeline-org-mode)
  (add-hook 'org-agenda-mode-hook #'nano-modeline-org-agenda-mode))
