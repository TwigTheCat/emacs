;;; -*- lexical-binding: t; -*-

;; DENOTE

(use-package denote
  :defer t
  :config
  (setq denote-directory (expand-file-name "~/Documents/notes/denote/"))
  (setq denote-known-keywords '())
  (setq denote-file-type nil)
  (add-hook 'dired-mode-hook #'denote-dired-mode))

;; ORG-AGENDA

(setq org-agenda-files '("~/Documents/notes/agenda.org"))
(setq-default org-agenda-include-diary t)

;; ORG-SUPERSTAR

(use-package org-superstar
  :defer t
  :hook
  (org-mode . org-superstar-mode)
  :config
  (setq org-superstar-special-todo-items t)
  (setq org-superstar-headline-bullets-list '("󱙧"
					                          "☭"
					                          "❦"
					                          ""
					                          "󰢄"))
  ;; disables leading bullets
  (setq org-superstar-leading-bullet ?\s)
  (setq org-indent-mode-turns-on-hiding-stars nil))

;; ORG TABLE OF CONTENTS

(use-package toc-org
  :defer t
  :hook
  (org-mode . toc-org-mode)
  :commands toc-org-enable)

;; ORG AUTO-TANGLE

(use-package org-auto-tangle
  :defer t
  :hook
  ((org-mode . org-auto-tangle-mode))
  :config
  (setq org-auto-tangle-default t))

;; LATEX

(use-package auctex
  :defer t)

(use-package cdlatex
  :defer t)

(use-package org-fragtog
  :defer t
  :custom
  (org-startup-with-latex-preview t)
  :hook
  ((org-mode . org-fragtog-mode)))

(with-eval-after-load 'org-mode
  (plist-put org-format-latex-options :scale 1.5))

;; ORG-APPEAR

(use-package org-appear
  :defer t
  :hook
  (org-mode . org-appear-mode))

;; ORG-ALERT

(use-package org-alert
  :defer t
  :config
  (setq org-alert-enable t)
  (setq alert-default-style 'libnotify)
  (setq org-alert-interval 150))

;; ORG-TEMPO

(require 'org-tempo)

;; ORG-MODE

(setq org-display-custom-times t)
(setq org-pretty-entities t)
(setq org-use-sub-superscripts "{}")
(setq org-hide-emphasis-markers t)
(setq org-startup-with-inline-images t)
(setq org-return-follows-link t)
;; Stop src blocks from auto indenting
(setq org-edit-src-content-indentation 0)

(add-hook 'org-mode-hook 'org-indent-mode)
