;;; -*- lexical-binding: t; -*-

;; MAGIT

(use-package transient
  :defer t)
(use-package magit
  :defer t)

;; DIRENV

(use-package direnv
  :config
  (direnv-mode)
  (setq direnv-always-show-summary nil))

;; WHICH-KEY

(require 'which-key)
(which-key-mode 1)
(setq which-key-sort-uppercase-first nil
      which-key-allow-imprecise-window-fit t
      which-key-max-description-length 25
      which-key-sort-order #'which-key-key-order-alpha
      which-key-sort-uppercase-first nil
      which-key-add-column-padding 1
      which-key-max-display-columns nil
      which-key-min-display-lines 6)

;; NEOTREE

(use-package neotree
  :defer t
  :config
  (setq neo-theme (if (display-graphic-p) 'nerd-icons 'arrow))
  (setq neo-smart-open t
        neo-show-hidden-files t
        neo-window-width 30
        neo-window-fixed-size nil
        inhibit-compacting-font-caches t
        projectile-switch-project-action 'neotree-projectile-action) 
        ;; truncate long file names in neotree
        (add-hook 'neo-after-create-hook
           #'(lambda (_)
               (with-current-buffer (get-buffer neo-buffer-name)
                 (setq truncate-lines t)
                 (setq word-wrap nil)
                 (make-local-variable 'auto-hscroll-mode)
                 (setq auto-hscroll-mode nil)))))

;; VTERM

(use-package vterm
  :defer t)

(use-package vterm-toggle
  :defer t
  :after vterm
  :config
  ;; When running programs in Vterm and in 'normal' mode, make sure that ESC
  ;; kills the program as it would in most standard terminal programs.
  (evil-define-key 'normal vterm-mode-map (kbd "<escape>") 'vterm--self-insert)
  (setq vterm-toggle-fullscreen-p nil)
  (setq vterm-toggle-scope 'project)
  (add-to-list 'display-buffer-alist
               '((lambda (buffer-or-name _)
                     (let ((buffer (get-buffer buffer-or-name)))
                       (with-current-buffer buffer
                         (or (equal major-mode 'vterm-mode)
                             (string-prefix-p vterm-buffer-name (buffer-name buffer))))))
                  (display-buffer-reuse-window display-buffer-at-bottom)
                  ;;(display-buffer-reuse-window display-buffer-in-direction)
                  ;;display-buffer-in-direction/direction/dedicated is added in emacs27
                  ;;(direction . bottom)
                  ;;(dedicated . t) ;dedicated is supported in emacs27
                  (reusable-frames . visible)
                  (window-height . 0.4))))

;; SAVEHIST

(use-package savehist
  :init
  (savehist-mode))

;; UNDO-TREE

(use-package undo-tree
  :config
  (global-undo-tree-mode)
  ;; I don't want random files in my directories for the undo-tree,
  ;; so unify them under this directory
  (setq undo-tree-history-directory-alist '(("." . "~/.config/emacs/undo-tree"))))

;; DASHBOARD

(use-package dashboard
  :init
  (setq initial-buffer-choice 'dashboard-open)
  (setq dashboard-projects-backend 'projectile)
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  (setq dashboard-banner-logo-title "MRROW MRRP")
  (setq dashboard-startup-banner "")  ;; use custom image as banner
  (setq dashboard-center-content t) ;; set to 't' for centered content
  (setq dashboard-items '((recents . 5)
                          (projects . 5)
                          (agenda . 5)))
  :custom 
  (dashboard-modify-heading-icons '((recents . "file-text")
				      (bookmarks . "book")))
  :config
  (dashboard-setup-startup-hook))

;; PROJECTILE

(use-package projectile
  :config
  (projectile-mode +1))
