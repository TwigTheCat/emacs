;;; -*- lexical-binding: t; -*-

;; INDENTATION

(setq-default indent-tabs-mode nil)
(setq-default tab-width 2)
(electric-indent-mode 1)

;; AUTO PARENS

(electric-pair-mode 1)

;; TREESIT

(use-package treesit-auto
  :defer t
  :config
  (setq treesit-font-lock-level 4)
  (global-treesit-auto-mode)
  (treesit-auto-add-to-auto-mode-alist 'all)
  (setq treesit-auto-install t)
  (add-to-list 'treesit-auto-recipe-list
               (make-treesit-auto-recipe
		        :lang 'zig
 	            :ts-mode 'zig-ts-mode
 		        :remap 'zig-mode
 		        :url "https://github.com/maxxnino/tree-sitter-zig"
 		        :ext "\\.\\(zig\\|zon\\)\\'")))

;; EGLOT

(require 'eglot)
(with-eval-after-load 'eglot
  (add-to-list 'eglot-server-programs '(zig-ts-mode . ("zls")))
  (add-to-list 'eglot-server-programs '(nix-ts-mode . ("nil")))
  (add-hook 'rust-ts-mode-hook 'eglot-ensure)
  (add-hook 'mhtml-mode-hook 'eglot-ensure)
  (add-hook 'zig-ts-mode-hook 'eglot-ensure)
  (add-hook 'nix-ts-mode-hook 'eglot-ensure)
  (setq completion-category-overrides '((eglot (styles orderless))
                                        (eglot-capf (styles orderless))))
  ;; (advice-add 'eglot-completion-at-point :around #'cape-wrap-buster)
  ;; (setq-local completion-at-point-functions (list #'eglot-completion-at-point))
  (add-hook 'eglot-managed-mode-hook #'eldoc-box-hover-mode t)
  (add-hook 'before-save-hook 'eglot-format-buffer))
(setq read-process-output-max (* 1024 1024))

(use-package eldoc-box
  :config
  (require 'eldoc-box))

;; SNIPPETS

(use-package yasnippet)

;; GOLANG

(add-hook 'go-ts-mode-hook
          (lambda ()
            (lsp-mode)
            (gofmt-before-save)
            (setq-local electric-indent-mode nil)
            (setq-local tab-width 4)
            (setq-local go-ts-mode-indent-offset nil)
            (setq-local indent-line-function 'insert-tab)))

;; RUST

(use-package rust-mode
  :defer t
  :init
  (setq rust-mode-treesitter-derive t)
  (setq rust-format-on-save t)
  :config
  (add-hook 'rust-ts-mode-hook #'lsp)
  (add-hook 'rust-ts-mode-hook
            (lambda () (prettify-symbols-mode)))
  (add-hook 'rust-ts-mode-hook
          (lambda () (setq indent-tabs-mode nil))))

;; YAML

(use-package yaml-mode
  :defer t)

;; JSON

(use-package json-mode
  :defer t)

;; HASKELL

(use-package haskell-mode
  :defer t)

(use-package hindent
  :defer t
  :hook (haskell-mode . hindent-mode))

;; VLANG

(use-package v-mode
  :defer t)

;; NIX

(use-package nix-mode
  :defer t)

(use-package nix-ts-mode
  :mode "\\.nix\\'"
  :defer t)

;; ZIG

(straight-use-package '(zig-ts-mode
  :type git
  :host codeberg
  :repo "meow_king/zig-ts-mode"
  ))
(add-to-list 'auto-mode-alist '("\\.zig\\'" . zig-ts-mode))

;; MARKDOWN

(use-package markdown-mode
    :hook ((markdown-mode . visual-line-mode)))

;; QML

;; (setq qml-ts-mode-indent-offset 2)
;; (straight-use-package '(qml-ts-mode
;;   :type git
;;   :host github
;;   :repo "outfoxxed/qml-ts-mode"
;;   :config
;;   (add-hook 'qml-ts-mode-hook
;;             (lambda () (setq-local electric-indent-chars '(?\n ?\( ?\) ?{ ?} ?\[ ?\] ?\; ?,))))))

;; FORMATTING

(use-package reformatter
  :config
  (reformatter-define nix-alejandra :program "alejandra")
  (add-hook 'nix-ts-mode-hook 'nix-alejandra-on-save-mode))

;; FLYCHECK/FLYSPELL

;; (use-package flycheck
;;   :defer t
;;   :diminish
;;   :config (global-flycheck-mode))

;; (add-hook 'text-mode-hook 'flyspell-mode)
;; (add-hook 'prog-mode-hook 'flyspell-prog-mode)

;; OCAML

(use-package tuareg
  :defer t)
(use-package dune
  :defer t)
(use-package merlin
  :defer t
  :config
  (add-hook 'tuareg-mode-hook #'merlin-mode))

;; This uses Merlin internally
;; (use-package flycheck-ocaml
;;   :config
;;   (flycheck-ocaml-setup))

(use-package utop
  :defer t
  :config
  (add-hook 'tuareg-mode-hook #'utop-minor-mode))

(use-package ocp-indent
  :defer t
  :config
  (add-to-list 'load-path "/home/ly/.opam/default/share/emacs/site-lisp")
  (require 'ocp-indent))

