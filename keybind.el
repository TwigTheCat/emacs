;;; -*- lexical-binding: t; -*-

;; YES/Y NO/N

(fset 'yes-or-no-p 'y-or-n-p)

;; WINDMOVE

(windmove-default-keybindings)

;; EVIL

(use-package evil
  :init
  (setq evil-want-keybinding nil)
  (setq evil-vsplit-window-right t)
  (setq evil-split-window-below t)
  (evil-mode)
  :config
  ;; Delete without register
  (evil-define-operator evil-delete-without-register (beg end type yank-handler)
    (interactive "<R><y>")
    (evil-delete beg end type ?_ yank-handler))
  (define-key evil-insert-state-map (kbd "C-<backspace>") 'evil-delete-backward-word)

  (with-eval-after-load 'evil-maps
    (define-key evil-motion-state-map (kbd "SPC") nil)
    (define-key evil-motion-state-map (kbd "RET") nil)
    (define-key evil-motion-state-map (kbd "TAB") nil))

  (evil-set-undo-system 'undo-tree))

(use-package evil-collection
  :after evil
  :config
  (setq evil-collection-mode-list '(dashboard dired ibuffer consult ))
  (evil-collection-init))

;; GENERAL

(use-package general
  :config
  (general-evil-setup)

  ;; set up 'SPC' as the global leader key
  (general-create-definer twig/leader-keys
    :states '(normal insert visual emacs)
    :keymaps 'override
    :prefix "SPC" ;; set leader
    :global-prefix "M-SPC") ;; access leader in insert mode

  (twig/leader-keys
    "b" '(:ignore t :wk "buffer")
    "bb" '(switch-to-buffer :wk "Switch buffer")
    "bk" '(evil-delete-buffer :wk "Kill this buffer")
    "bn" '(next-buffer :wk "Next buffer")
    "bp" '(previous-buffer :wk "Previous buffer")
    "br" '(revert-buffer :wk "Reload buffer"))
  (twig/leader-keys
    "t"  '(:ignore t :wk "neotree")
    "to" '(neotree-toggle :wk "Open Neotree"))
  (twig/leader-keys
    "v"  '(:ignore t :wk "vterm")
    "vt" '(vterm-toggle :wk "Toggle Vterm")))

;; OTHER KEYBINDS

;; zoom in and out
(global-set-key (kbd "C-=") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "<C-wheel-up>") 'text-scale-increase)
(global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease)

;; drag stuff
(global-set-key (kbd "M-<up>") 'drag-stuff-up)
(global-set-key (kbd "M-<down>") 'drag-stuff-down)

;; man fuck minibuffers and their 3 esc quits
(global-set-key [escape] 'keyboard-escape-quit)

(global-set-key (kbd "C-c c") 'comment-line)
